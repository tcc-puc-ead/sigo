import { Component, OnInit } from '@angular/core';
import { TechNormModel } from '../../models/technorm.model';
import { TechNormService } from '../../services/technorm/technorm.service';
import { ActivatedRoute, Router } from "@angular/router";
import { LoadingControlService } from 'src/app/services/shared/loadingControl.service';

@Component({
  selector: 'app-tech-norm-detail',
  templateUrl: './tech-norm-detail.component.html',
  styleUrls: ['./tech-norm-detail.component.css']
})
export class TechNormDetailComponent implements OnInit {

  loadingComponent:boolean;
  normModel: TechNormModel;
  
  constructor(private techNormService: TechNormService, private router: Router, 
              private activatedRouter: ActivatedRoute,
              private loadingService: LoadingControlService) { 
  }

  ngOnInit(): void {
    this.normModel = new TechNormModel('','','');
    let normId = this.activatedRouter.snapshot.params['id']

    if(normId){
        this.getNormById(normId);
    }
    
  }

  saveNorm(formModel:any){
    this.loadingService.setLodingWorking(this.loadingComponent);
    
    this.normModel.normId ? this.updateNorm(formModel) : this.createNorm(formModel);
  }

  //#region Private Methods
  private getNormById(normId:string){
    this.techNormService.getNormById(normId).subscribe(
      (resSuccess) => {
        this.normModel = new TechNormModel(resSuccess.normTitle, 
                                            resSuccess.competentOrganization,
                                            resSuccess.normBody,
                                            resSuccess.normId);
      },
      (resError) => {
        console.log(resError.message);
      }
    );
  }
  
  private createNorm(formModel:any){
    let model = new TechNormModel(formModel.normTitle, formModel.competentOrgan, formModel.normBody);

    this.techNormService.createNorm(model).subscribe(
      (resSuccess) => {
        this.router.navigate(["/norms"]);
      },
      (resError) =>{
        console.log(resError.message);
      }
    ).add(() => {
      this.loadingService.setLodingFinish(this.loadingComponent);
    });
  }

  private updateNorm(formModel:any){
    
    this.normModel.setTitle(formModel.normTitle);
    this.normModel.setCompetentOrg(formModel.competentOrgan);
    this.normModel.setBody(formModel.normBody);
    
    this.techNormService.updateNorm(this.normModel).subscribe(
          (resSuccess) => {
            this.router.navigate(["/norms"]);
          },
          (resError) =>{
            console.log(resError.message);
          }
        ).add(() => {
          this.loadingService.setLodingFinish(this.loadingComponent);
        });
  }
  //#endregion

}
