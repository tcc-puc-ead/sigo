import { Component, OnInit } from '@angular/core';
import { TechNormModel } from 'src/app/models/technorm.model';
import { TechNormService } from '../../services/technorm/technorm.service';
import { LoadingControlService } from '../../services/shared/loadingControl.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tech-norm',
  templateUrl: './tech-norm.component.html',
  styleUrls: ['./tech-norm.component.css']
})
export class TechNormComponent implements OnInit {

  constructor(private service: TechNormService, private loadingService: LoadingControlService,
              private router: Router) { }
  norms: TechNormModel[];
  loadingComponent:boolean;
  resultHasNoItens:boolean = false;

  ngOnInit(): void {
    this.loadingComponent = true;
    this.loadNormList();
  }

  loadNormList(){
    this.service.listTechNorm().subscribe(
      (resSucess) => {
        this.norms = resSucess;
      },
      (resError) =>{
        console.log(resError.message);
        this.calResultHasNoItensValue();
      }
    ).add(() => {
      this.afterServiceCalls();
    });
  }

  editNorm(item: TechNormModel){
    this.router.navigate([`/normdetail/${item.normId}`]);
  }

  deleteNorm(item: TechNormModel){
    this.loadingComponent = true
    this.service.deleteTechNorm(item.normId).subscribe(
      (resSuccess) => {
        this.loadNormList();
      },
      (resError) => {
        console.log(resError.message);
      }
    ).add(() => {
      this.afterServiceCalls();
    });
  }

  searchForName(formValue:any){
    
    this.service.listTechNormByTitle(formValue.normTitle).subscribe(
      (resSucess) => {
        this.norms = resSucess;
      },
      (resError) =>{
        console.log(resError.message);
        this.calResultHasNoItensValue();
      }
    ).add(() => {
      this.afterServiceCalls();
    });
  }

  private afterServiceCalls(){
    this.calResultHasNoItensValue();
    this.loadingComponent = false;
    console.log(this.resultHasNoItens);
  }

  private calResultHasNoItensValue(){
    this.resultHasNoItens = this.norms ? this.norms.length === 0 : true;
  }

}
