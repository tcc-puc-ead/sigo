import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment as env } from '../../../environments/environment';
import { TechNormModel } from '../../models/technorm.model';


@Injectable({
    providedIn: 'root',
})
export class TechNormService {

    constructor(private http: HttpClient) {}

    auth_token:string = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjRfNi14SUkxZDZtMFRXQ1VMUmRZWiJ9.eyJpc3MiOiJodHRwczovL2Rldi01cWF2a3RzOC51cy5hdXRoMC5jb20vIiwic3ViIjoiM2pZNGtMWlRhS3E4YUdwWnJZdlEyOVY2VXJtYzNRVk1AY2xpZW50cyIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzAwMy8iLCJpYXQiOjE2MzA0MTA5MzcsImV4cCI6MTYzMDQ5NzMzNywiYXpwIjoiM2pZNGtMWlRhS3E4YUdwWnJZdlEyOVY2VXJtYzNRVk0iLCJndHkiOiJjbGllbnQtY3JlZGVudGlhbHMifQ.wOyv_sXo2Df75So1UAlRYBxy-wx6YUluOwWjLG0fCVgNWe56b-rBJWUnn4bdL1K5iq3CHXNe6Bxo-u9tFr2N6Xz4dFebdJ0MhLJZV3thta0GfS24HT-x00ZE5GzSs162TOAIcOG2YfsGEQzVaPm40XDiLLoIFlDk27BRdv_PXuVZu_bnA_iC1gZGol1cr6EJdRDk9lGhIEpfQtguGV14wXLJECSHAiBWxVz_KIRjhBDF_k297d4o80xJyHiTjj3IM8tiTeQ27q-eMY4leDIjCqUfzk54ZYS_QQJ6kaa_lQO2CRFeKe0AUkkRXcinpWZULQH3SZSMInx0bWTSTVVj_g'
    headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.auth_token}`
    })


    listTechNorm(): Observable<TechNormModel[]> {
        
        return this.http.get<TechNormModel[]>(`${env.apiUri}/api/technorms/`, {headers: this.headers});
    }

    listTechNormByTitle(title:string): Observable<TechNormModel[]> {
        
        return this.http.get<TechNormModel[]>(`${env.apiUri}/api/technormsbytitle/${title}`, {headers: this.headers});
    }

    getNormById(id: string): Observable<TechNormModel> {
        return this.http.get<TechNormModel>(`${env.apiUri}/api/technorms/${id}`, {headers: this.headers});
    }

    createNorm(model: TechNormModel): Observable<TechNormModel> {

        return this.http.post<TechNormModel>(`${env.apiUri}/api/technorm/`, model, {headers: this.headers});
    }

    updateNorm(model: TechNormModel):Observable<TechNormModel>{
        console.log(model);
        return this.http.put<TechNormModel>(`${env.apiUri}/api/technorm/`, model, {headers: this.headers});
    }

    deleteTechNorm(idNorm:string) {
        return this.http.delete<any>(`${env.apiUri}/api/technorm/${idNorm}`, {headers: this.headers});
    } 

    

}