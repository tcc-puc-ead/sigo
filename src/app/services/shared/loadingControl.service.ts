
import { Injectable } from '@angular/core';
@Injectable({
    providedIn: 'root',
})
export class LoadingControlService{
    
    
    public setLodingWorking(loadingControl:boolean){
        loadingControl = true;
    }

    public setLodingFinish(loadingControl:boolean){
        loadingControl = false;
    }
    
    public afterServiceCalls(loadingControl:boolean, entryFunction:any){
          entryFunction();
          this.setLodingFinish(loadingControl);
    }
}