import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { ErrorComponent } from './pages/error/error.component';
import { AuthGuard } from '@auth0/auth0-angular';
import { TechNormComponent } from './pages/tech-norm/tech-norm.component';
import { NotImplementedComponent } from './pages/not-implemented/not-implemented.component';
import { TechNormDetailComponent } from './pages/tech-norm-detail/tech-norm-detail.component';

const routes: Routes = [
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'norms',
    component: TechNormComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'normdetail',
    component: TechNormDetailComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'normdetail/:id',
    component: TechNormDetailComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'consultant',
    component: NotImplementedComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'managment',
    component: NotImplementedComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'error',
    component: ErrorComponent,
  },
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
