export class TechNormModel{
    normId: string
    normTitle: string
    competentOrganization: string
    normBody: string

    constructor(title:string, competentOrgan:string, textNorm:string, id:string = ''){
        this.normTitle = title
        this.competentOrganization = competentOrgan
        this.normBody = textNorm
        this.normId = id
    }

    setTitle(title:string):void{
        if(title){
            this.normTitle = title;
        }
    }

    public setCompetentOrg(org:string){
        if(org){
            this.competentOrganization = org;
        }
    }

    public setBody(body:string){
        if(body){
            this.normBody = body;
        }
    }
}